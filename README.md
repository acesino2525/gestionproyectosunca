# gestionProyectosUnca


## El siguiente proyecto será dividido en etapas que luego tendrán sub etapas.
- [ ] Analizar el proyecto y traspolarlo a un diagrama:
    - [ ] Qué funcionalidad cumple la web
    - [ ] Definir qué tipo de datos recibirá y mostrará la web
    - [ ] Diagramar la arquitectura en base
- [ ] Diagramar la distribución de la web (FRONT) y las páginas necesarias
    - [x] Crear los .html con las rutas básicas
    - [x] Vincular a css local
    - [ ] ... agregar más items a medida que avancemos a este paso y distribuir metas
- [ ] Estructurar la base de datos en Postgres
- [ ] Planificar la API backend
